package com.tmarsteel.jecs.system

import com.tmarsteel.jecs.EntitySystem

/**
 * Provides thread-safety and convenience assertions to [ComponentSystem] by turning all its methods in to SPI methods.
 * @author Tobias Marstaller
 */
abstract class DefaultComponentSystem: ComponentSystem {
    private var _entitySystem: EntitySystem? = null

    private val registrationLock = Any()
    private val tickLock = Any()

    protected val entitySystem: EntitySystem
        get() = _entitySystem ?: throw IllegalStateException("Not registered with any EntitySystem")

    final override fun onRegister(entitySystem: EntitySystem) {
        synchronized(registrationLock) {
            if (_entitySystem != null) {
                if (_entitySystem !== entitySystem) {
                    throw IllegalStateException("This ComponentSystem is already registered with another EntitySystem")
                }
                return
            }

            _entitySystem = entitySystem
            onRegister()
        }
    }

    final override fun onUnregister(entitySystem: EntitySystem) {
        synchronized(registrationLock) {
            if (_entitySystem == null || _entitySystem !== entitySystem) {
                throw IllegalStateException("This ComponentSystem is not registered with the given EntitySystem")
            }

            // dont unregister during a tick
            synchronized(tickLock) {
                onUnregister()
                _entitySystem = null
            }
        }
    }

    final override fun tick(entitySystem: EntitySystem, tickDuration: Float) {
        if (_entitySystem == null || _entitySystem != entitySystem) {
            throw IllegalStateException("This ComponentSystem is not registered with the given EntitySystem")
        }

        synchronized(tickLock) {
            onTick(tickDuration)
        }
    }

    /**
     * Called after this [ComponentSystem] has been registered with an [EntitySystem]
     */
    protected open fun onRegister(): Unit {}

    /**
     * Called before this [ComponentSystem] is unregistered from [EntitySystem] is hat previously been registered with
     * (see [onRegister]).
     */
    protected open fun onUnregister(): Unit {}

    /**
     * Calculates a tick. It is assured that this method is never called while this [ComponentSystem] is not registered
     * with any [EntitySystem]. It is also assured that [entitySystem] is the same object as has previously been passed
     * to [onRegister].
     * @param tickDuration The duration of the tick in seconds (for time-relevant calculations)
     */
    protected abstract fun onTick(tickDuration: Float): Unit
}