package com.tmarsteel.jecs.system

import com.tmarsteel.jecs.EntitySystem

/**
 * Interface for component systems. Systems should subclass [DefaultComponentSystem] instaed of this interface
 * @author Tobias Marstaller
 */
interface ComponentSystem {
    /**
     * Called by `es` when it processes a tick. The method should finish within `tickDuration`
     * seconds because otherwise the [EntitySystem] gets out of clock-sync or excludes this system from the
     * tick.
     * @param es The EntitySystem processing a tick.
     * *
     * @param tickDuration The duration of the tick in seconds.
     */
    fun tick(es: EntitySystem, tickDuration: Float)

    /**
     * Called by an [EntitySystem] after this system has been registered with it
     * @see EntitySystem.register(ComponentSystem)
     */
    fun onRegister(es: EntitySystem) {}

    /**
     * Called by an [EntitySystem] before this system is unregistered from the calling [EntitySystem]
     */
    fun onUnregister(es: EntitySystem) {}
}
