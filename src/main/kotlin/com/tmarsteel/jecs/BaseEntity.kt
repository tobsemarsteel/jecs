package com.tmarsteel.jecs

/**
 * Base class for [Entity] implementations. Implements the absolute minimum of functionality and reduces the amount
 * of doubled code when many different entity classes are created.
 * @param entitySystem The entity system this entity is to be registered at.
 * @author Tobias Marstaller
 */
open class BaseEntity(override val entitySystem: EntitySystem) : Entity
