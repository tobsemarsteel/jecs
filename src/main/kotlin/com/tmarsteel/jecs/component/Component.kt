package com.tmarsteel.jecs.component

import com.tmarsteel.jecs.Entity

/**
 * Base interface for all components.
 * @author Tobias Marstaller
 */
interface Component {

    /**
     * The type of this component. Used to override standard components with specialised functionality.
     */
    val type: Class<*>
        get() = this.javaClass

    /**
     * Called by the [com.tmarsteel.jecs.EntitySystem] before this component is registered with the given entity.
     * Components need not support being registered with multiple [Entity]s. However, throwing a
     * [UnsupportedOperationException] in that case increases the usability.
     * @param e The entity this component has just been registered with.
     */
    fun onRegister(e: Entity) {

    }

    /**
     * Called by the [com.tmarsteel.jecs.EntitySystem] after this component has been unregistered from the given
     * entity.
     * @param e The entity this component has just been unregistered with.
     */
    fun onUnregister(e: Entity) {

    }
}
