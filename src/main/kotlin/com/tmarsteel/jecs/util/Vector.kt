package com.tmarsteel.jecs.util

/**
 * A simple 3D vector with basic arithmetic methods.
 * X is the horizontal, y the vertical and z the depth axis.
 * @author Tobias Marstaller @gmail.com>
 */
data class Vector(val x: Double, val y: Double, val z: Double) {

    /**
     * The length of this vector. Mathematically this is sqrt(x * x + y * y + z * z).
     */
    val length : Double get() = java.lang.Math.sqrt(x * x + y * y + z * z)

    /**
     * Returns the inverse of this vector. Mathematically this is (-x, -y, -z).
     * @return The inverse of this vector
     */
    fun inverse(): Vector = Vector(-x, -y, -z)

    /**
     * Adds the given vector to this vector and returns the result. Mathematically this is (x + v.x, y + v.y, z + v.z)
     * @param v The vector to add.
     * *
     * @return The result of the vector addition.
     */
    fun add(v: Vector): Vector {
        return if (v === Vector.ORIGIN) this else if (this === Vector.ORIGIN) v else Vector(v.x + x, v.y + y, v.z + z)
    }

    operator fun plus(v: Vector) = add(v)

    /**
     * Subtracts the given vector from this vector and returns the result. Mathematically this is (x - v.x, y - v.y, z - v.z)
     * @param v The vector to subtract.
     * *
     * @return The result of the vector subtraction
     */
    fun subtract(v: Vector): Vector {
        return if (v === Vector.ORIGIN) this else Vector(x - v.x, y - v.y, z - v.z)
    }

    operator fun minus(v: Vector) = subtract(v)

    /**
     * Multiplies this vector with the given scalar factor. Mathematically this is (x * f, y * f, z * f)
     * @param f The factor to multiply with
     * *
     * @return The result of the vector-scalar multiplication.
     */
    fun multiply(f: Double): Vector = Vector(x * f, y * f, z * f)

    operator fun times(f: Double) = multiply(f)
    operator fun times(f: Float)  = multiply(f.toDouble())

    companion object {
        /**
         * A vector with x = y = z = 0
         */
        public val ORIGIN = Vector(0.0, 0.0, 0.0)
    }
}