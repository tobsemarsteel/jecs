package com.tmarsteel.jecs

import com.tmarsteel.jecs.component.Component

/**
 * Superinterface for entities. The default methods of this interface must not be overwritten; if they are overwritten
 * there is no guarantee that [EntitySystem] will work as designed.
 * @author Tobias Marstaller @gmail.com>
 */
interface Entity {

    val entitySystem : EntitySystem

    /**
     * Adds the given component to this entity and registers it with the entity system.
     * @param component The component to add
     *
     * @throws ComponentTypeAlreadyRegisteredException If a component of type `asType` is already registered
     */
    @Throws(ComponentTypeAlreadyRegisteredException::class)
    fun <T : Component> addComponent(component: T) = addComponent(component, component.javaClass)

    /**
     * Adds the given component to this entity as type `asType`. Use this method for subclasses of main
     * components (e.g. class X extends HealthComponent: addComponent(new X(), HealthComponent.class))
     * @param component The component to add
     * @param asType The to add the component as, useful for subclasses of components
     *
     * @throws ComponentTypeAlreadyRegisteredException If a component of type `asType` is already registered
     * @throws AssertionError If `component` is not an instance of `asType`
     */
    @Throws(ComponentTypeAlreadyRegisteredException::class)
    fun <T : Component> addComponent(component: T, asType: Class<*>) = entitySystem.registerComponent(this, component, asType)

    /**
     * Returns the component of type T associated with this entity or null if no such component is registered.
     * @param ofType The type of component to return
     *
     * @return The component of type T or null if no such component is registered with this entity.
     * @throws NoSuchComponentException If a component of the requested type is not registered with this entity
     */
    @Throws(NoSuchComponentException::class)
    fun <T : Component> getComponent(ofType: Class<T>): T = entitySystem.getComponent(this, ofType)
}