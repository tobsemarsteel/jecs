package com.tmarsteel.jecs

import com.tmarsteel.jecs.component.Component

class NoSuchComponentException(message: String, cause: Throwable? = null) : RuntimeException(message, cause) {
    constructor(componentType: Class<out Component>) : this("No component of type ${componentType.javaClass.name} registered")
    constructor(componentType: Class<out Component>, entity: Entity) : this("No component of type ${componentType.javaClass.name} registered for entity $entity")
}