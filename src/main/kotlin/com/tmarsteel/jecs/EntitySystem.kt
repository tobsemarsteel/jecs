package com.tmarsteel.jecs

import com.tmarsteel.jecs.component.Component
import com.tmarsteel.jecs.system.ComponentSystem
import java.util.*
import java.util.function.Consumer

/**
 * A Entity-Component-System. Manages [Entity]s and their Components (e.g. [com.tmarsteel.jecs.system.health.HealthComponent]).
 * @author Tobias Marstaller @gmail.com>
 */
class EntitySystem {
    /**
     * Maps entities to maps of their components.
     */
    private val componentsByEntities : MutableMap<Entity, MutableMap<Class<*>, Component>> = HashMap()

    /**
     * Maps entities by component types (to speed up the each method).
     * The type of the value is [HashSet] because the [Cloneable.clone] method is required by
     * [.each].
     */
    private val entitiesByComponentTypes : MutableMap<Class<*>, HashSet<Entity>> = HashMap()

    /**
     * All systems currently registered with this entity system.
     */
    private val componentSystems : MutableSet<ComponentSystem> = HashSet()

    /**
     * The desired execution order of [ComponentSystem] types; as defined from outside this class by
     * [.assureExecutionOrder].
     */
    private var desiredExecutionOrder: Array<Class<out ComponentSystem>>? = null

    /**
     * The actual execution order during [.tick]. This variable is recalculated by [.rebuildExecutionOrder]
     * whenever:
     *
     *  * A new component is added using [.register]
     *  * a component is unregistered using [.unregister]
     *  * [.assureExecutionOrder] is called
     *
     */
    private var actualExecutionOrder: List<ComponentSystem>? = null

    /**
     * Registers the given entity with this system.
     * @param e The entity to register
     */
    @Synchronized fun register(e: Entity) {
        if (!this.componentsByEntities.containsKey(e)) {
            this.componentsByEntities.put(e, HashMap<Class<*>, Component>())
        }
    }

    /**
     * Removes the entity from this system, dropping all references to it. Note that this essentially makes the entity
     * useless.
     * @param e The entity to unregister
     */
    @Synchronized fun unregister(e: Entity) {
        this.componentsByEntities.remove(e)

        this.entitiesByComponentTypes.values.forEach { it.remove(e) }
    }

    /**
     * Registers the given component system with this entity component system. It will then be triggered on every tick
     * of this system.
     * @param s The system to register
     */
    @Synchronized fun register(s: ComponentSystem) {
        if (this.componentSystems.add(s)) {
            this.rebuildExecutionOrder()
        }
    }

    /**
     * Unregisters the given component system, dropping all references to it.
     * @param s The system to unregister
     */
    @Synchronized fun unregister(s: ComponentSystem) {
        if (this.componentSystems.remove(s)) {
            this.rebuildExecutionOrder()
        }
    }

    /**
     * Registers the given component with the given entity.
     * @param e The entity to associate the given component with. If not registered with this system yet the entity will
     * *          be registered.
     * @param o The component to register.
     * @throws ComponentTypeAlreadyRegisteredException If a component of type `asType` is already registered
     * * with the given entity.
     * @throws ComponentTypeAlreadyRegisteredException If a component of the same type is already registered
     */
    @Throws(ComponentTypeAlreadyRegisteredException::class)
    fun <T : Component> registerComponent(e: Entity, o: T) {
        registerComponent(e, o, o.type)
    }

    /**
     * Registers the given component with the given entity.
     * @param e The entity to associate the given component with. If not registered with this system yet the entity will
     * *          be registered.
     * @param c The component to register.
     * @param asType The type to register the component as. This is useful for subclassed components,
     * *               e.g. class X: HealthComponent: registerComponent(e, new X(), HealthComponent::class))
     * @throws ComponentTypeAlreadyRegisteredException If a component of type `asType` is already registered
     * with the given entity.
     * @throws AssertionError If `component` is not an instance of `asType`
     */
    @Synchronized @Throws(ComponentTypeAlreadyRegisteredException::class)
    fun <T : Component> registerComponent(e: Entity, c: T, asType: Class<*>) {
        assert(asType.isAssignableFrom(c.javaClass), { "Cannot register a component as a type of which it is not a subtype" })

        this.register(e) // this will assure that componentMap in the next statement is never null

        // map the component to the entity
        // assure that no more than one component of the same type is registered per entity
        val componentMap : MutableMap<Class<*>, Component> = this.componentsByEntities[e]!!
        val registered : Component? = componentMap[asType]
        if (registered != null) {
            if (registered === c) {
                // this component is already registered => we are done here
                return
            }

            // another component of the same type already registered => error
            throw ComponentTypeAlreadyRegisteredException(e, registered, c, asType)
        }

        c.onRegister(e)
        componentMap[asType] = c

        // map the entity to the component type
        if (this.entitiesByComponentTypes.containsKey(asType)) {
            this.entitiesByComponentTypes[asType]!!.add(e)
        } else {
            val entitySet = HashSet<Entity>()
            entitySet.add(e)
            this.entitiesByComponentTypes[asType] = entitySet
        }
    }

    /**
     * Unregisters the given component from the given entity.
     * **Important:** This method is slow. Use [.unregisterComponentType] whenever possible.
     * @param e The entity to unregister the component from
     *
     * @param component The component to unregister
     */
    @Synchronized fun unregisterComponent(e: Entity, component: Component) {
        val componentMap = this.componentsByEntities[e] ?: return // entity was not registered => we are done here

        // find the type of the component
        val mappings = componentMap.filter { it.value === component }

        mappings.forEach { entry ->
            val type = entry.key

            // remove the entity => component association
            componentMap.remove(type)

            // remove the component => entity association
            this.entitiesByComponentTypes[type]?.remove(e)

            component.onUnregister(e)
        }
    }

    /**
     * Unregisters the component of type `componentType` from the [Entity] `e`.
     * @param e The entity to unregister the component from
     *
     * @param componentType The type of the component to unregister
     */
    fun <T : Component> unregisterComponentType(e: Entity, componentType: Class<T>) {
        var removedComponent = componentsByEntities[e]?.remove(componentType as Class<in Component>)

        entitiesByComponentTypes[componentType as Class<in Component>]?.remove(e)

        removedComponent?.onUnregister(e)
    }

    /**
     * Returns the component of type `componentType` registered with the given entity or null if no
     * such component is registered (or the entity is not registered).
     * @param e The entity to query
     * @param componentType The type of component to get
     * @return The component or null if no such component is registered.
     * @throws NoSuchComponentException If a component of the requested type is not registered with the given entity.
     */
    @Throws(NoSuchComponentException::class)
    fun <T : Component> getComponent(e: Entity, componentType: Class<T>): T {
        val entityComponentMap = this.componentsByEntities[e] ?: throw NoSuchComponentException("Entity $e was never registered with this EntitySystem")

        return entityComponentMap[componentType] as T? ?: throw NoSuchComponentException(componentType, e)
    }

    /**
     * Returns the system of type `systemType` if registered with this [EntitySystem]
     * @param systemType The type of system to look for. Subclasses of this type will be matched, too.
     * @throws NoSuchSystemException If no system of the given type is registered with this [EntitySystem]
     */
    @Throws(NoSuchSystemException::class)
    fun <T : ComponentSystem> getComponentSystem(systemType: Class<T>): T {
        val system = this.componentSystems.find { systemType.isAssignableFrom(it.javaClass) } ?: throw NoSuchSystemException(systemType)

        return system as T
    }

    /**
     * Calls consumer.consume(entity) for all entities in this system for which components of all the types in
     * `requiredComponentTypes` are registered. It is assured that [.getComponent]
     * does not return null if called with an entity passed to `consumer` and one of the classes in
     * `requiredComponentTypes`.
     * **Performance notes**
     * The performance of this method is described in the following table. `e` is the number of entities
     * registered with this system, `n` is the number of entities affected by the criteria expressed by
     * `requiredComponentTypes` and `l` is the length of `requiredComponentTypes`.
     * <table>
     *   <tr>
     *       <th>l</th>
     *       <th>Performance O(...)</th>
     *   </tr>
     *   <tr>
     *       <td><code>0</code></td>
     *       <td><code>O(e)</code></td>
     *   </tr>
     *   <tr>
     *       <td><code>1</code></td>
     *       <td><code>O(n)</code></td>
     *   </tr>
     *   <tr>
     *       <td><code>&gt; 1</code></td>
     *       <td><code>O(l*n)</code></td>
     *   </tr>
     * </table>
     * @param requiredComponentTypes The component types to require / filter by
     */
    fun each(consumer : (Entity) -> Unit, vararg requiredComponentTypes: Class<out Component>) {
        if (requiredComponentTypes.size == 0) {
            // for all entities

            this.componentsByEntities.keys.forEach(consumer)
        } else if (requiredComponentTypes.size == 1) {
            // for specific entity type
            val entitySet = this.entitiesByComponentTypes[requiredComponentTypes[0] as Class<*>]

            entitySet?.forEach(consumer)
        } else {
            // find all entities for all component types and do an intersection of the sets
            var conjunctionSet: MutableSet<Entity>? = null

            for (componentType in requiredComponentTypes) {
                val entitySet = this.entitiesByComponentTypes[componentType as Class<*>]

                if (entitySet == null) {
                    // no entities fulfill this requirement => run empty set
                    conjunctionSet = null
                    break
                }

                if (conjunctionSet == null) {
                    conjunctionSet = entitySet.clone() as MutableSet<Entity>
                } else {
                    conjunctionSet.retainAll(entitySet)
                }
            }

            conjunctionSet?.forEach(consumer)
        }
    }

    /**
     * Java compatibility method for [.each]
     */
    fun each(consumer: Consumer<Entity>, vararg requiredComponentTypes: Class<out Component>) = each({entity -> consumer.accept(entity) }, *requiredComponentTypes)

    /**
     * Assures that the [ComponentSystem]s registered with this [EntitySystem] system are executed according
     * to the order of their types in `order` [each tick]. Past calls to this method will be ignored / forgotten
     * when this method is called again.
     * **Note:** A [ComponentSystem] will be called exactly once during a tick regardless of how many times
     * its type appears in `order`.
     * @param order The desired order in which to call [ComponentSystem] in [.tick].
     */
    fun assureExecutionOrder(vararg order: Class<out ComponentSystem>) {
        this.desiredExecutionOrder = arrayOf(*order)
        this.rebuildExecutionOrder()
    }

    /**
     * Renews [.actualExecutionOrder] according to the contents of [.desiredExecutionOrder] and [.componentSystems].
     */
    @Synchronized private fun rebuildExecutionOrder() {
        if (this.desiredExecutionOrder == null) {
            this.actualExecutionOrder = null
        } else {
            // execute the desired order first, then the rest
            val systemsToExecute = HashSet(this.componentSystems)
            val targetActualList = LinkedList<ComponentSystem>() // use linked list to speed up iteration

            // sort in the desired systems in order
            for (type in this.desiredExecutionOrder!!) {
                systemsToExecute.forEach { system ->
                    if (type.isAssignableFrom(system.javaClass) && !targetActualList.contains(system))
                        targetActualList.add(system)
                }
            }

            // remove the processed systems form systemsToExecute
            systemsToExecute.removeAll(targetActualList)

            // the remaining systems in systemsToExecute were not mentioned in #desiredExecutionOrder
            // they can be added to the array in any oder
            targetActualList.addAll(systemsToExecute)

            // store the result
            this.actualExecutionOrder = targetActualList
        }
    }

    /**
     * Performs a tick of duration `duration`. Calls tick(this, duration) on all component systems registered
     * with this entity system via [.register].
     * @param duration The duration of the tick (is used to calculate time-relevant values).
     */
    fun tick(duration: Float) {
        if (this.actualExecutionOrder == null) {
            // no particular order required => just loop through
            componentSystems.forEach { it.tick(this, duration) }
        } else {
            // iterate through actualExecutionOrder
            actualExecutionOrder!!.forEach { it.tick(this, duration) }
        }
    }

}