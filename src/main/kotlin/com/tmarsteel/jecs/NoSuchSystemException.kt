package com.tmarsteel.jecs

import com.tmarsteel.jecs.system.ComponentSystem

class NoSuchSystemException(message: String, cause: Throwable? = null) : RuntimeException(message, cause) {
    constructor(systemType: Class<out ComponentSystem>) : this("No system of type ${systemType.javaClass.name} registered")
}