package com.tmarsteel.jecs

/**
 * Created by tobia on 16.01.2016.
 */

/**
 * Thrown whenever a component is to be registered with an entity that already contains a component of the same type.
 * @author Tobias Marstaller @gmail.com>
 */
class ComponentTypeAlreadyRegisteredException(
    /**
     * The entity the component could not be registered with.
     */
    val targetEntity: Entity,
    /**
     * The component already assigned to the entity.
     */
    val existingComponent: Any,
    /**
     * The component that could not be added
     */
    val blockedComponent: Any,
    /**
     * The type common to `existingComponent` and `BlockedComponent`
     */
    val commonType: Class<*>) : RuntimeException("Could not register component with entity " + targetEntity.toString() + " because another component of the type " +
    commonType.name + " is already registered with that entity")
