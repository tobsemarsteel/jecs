package com.tmarsteel.jecs;

import com.tmarsteel.jecs.component.Component;
import com.tmarsteel.jecs.system.ComponentSystem;
import kotlin.Unit;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.function.Consumer;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author Tobias Marstaller <tobias.marstaller@gmail.com>
 */
public class EntitySystemTest
{
    @Test
    public void testGetComponentReturnsSameObjectAfterRegister()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        DummyComponent component = new DummyComponent();

        // ACT
        entitySystem.registerComponent(mockEntity, component, Component.class);

        // ASSERT
        assertSame(component, entitySystem.getComponent(mockEntity, Component.class));
    }

    @Test(expected = NoSuchComponentException.class)
    public void testGetComponentReturnsNullAfterComponentUnregister()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        DummyComponent component = new DummyComponent();

        // ACT
        entitySystem.registerComponent(mockEntity, component, Component.class);
        entitySystem.unregisterComponent(mockEntity, component);

        // ASSERT
        entitySystem.getComponent(mockEntity, Component.class);
    }

    @Test(expected = NoSuchComponentException.class)
    public void testGetComponentThrowsAfterComponentTypeUnregister()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        DummyComponent component = new DummyComponent();

        // ACT
        entitySystem.registerComponent(mockEntity, component, Component.class);
        entitySystem.unregisterComponentType(mockEntity, Component.class);

        // ASSERT
        entitySystem.getComponent(mockEntity, Component.class);
    }

    @Test
    public void forEachShouldPassEntity_SingleComponent()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Consumer<Entity> mockConsumer = mock(Consumer.class);

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, new DummyComponent());
        entitySystem.register(mockEntityNoise);

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, DummyComponent.class);

        // VERIFY
        verify(mockConsumer, times(1)).accept(mockEntity);
        verify(mockConsumer, never()).accept(mockEntityNoise);
    }

    @Test
    public void forEachShouldPassEntity_MultipleComponentsRegistered_SingleQueried()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Consumer<Entity> mockConsumer = mock(Consumer.class);

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, new DummyComponent());
        entitySystem.registerComponent(mockEntity, new SubDummyComponent());
        entitySystem.register(mockEntityNoise);
        entitySystem.registerComponent(mockEntityNoise, new DummyComponent());

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, SubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, times(1)).accept(mockEntity);
        verify(mockConsumer, never()).accept(mockEntityNoise);
    }

    @Test
    public void forEachShouldPassEntity_MultipleComponentsRegistered_AllQueried()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Consumer<Entity> mockConsumer = mock(Consumer.class);

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, new DummyComponent());
        entitySystem.registerComponent(mockEntity, new SubDummyComponent());
        entitySystem.register(mockEntityNoise);
        entitySystem.registerComponent(mockEntityNoise, new DummyComponent());

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, DummyComponent.class, SubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, times(1)).accept(mockEntity);
        verify(mockConsumer, never()).accept(mockEntityNoise);
    }

    @Test
    public void forEachShouldPassEntity_MultipleComponentsRegistered_SubsetQueried()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Consumer<Entity> mockConsumer = mock(Consumer.class);

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, new DummyComponent());
        entitySystem.registerComponent(mockEntity, new SubDummyComponent());
        entitySystem.registerComponent(mockEntity, new SubSubDummyComponent());
        entitySystem.register(mockEntityNoise);
        entitySystem.registerComponent(mockEntityNoise, new DummyComponent());
        entitySystem.registerComponent(mockEntityNoise, new SubDummyComponent());

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, SubDummyComponent.class, SubSubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, times(1)).accept(mockEntity);
        verify(mockConsumer, never()).accept(mockEntityNoise);
    }

    @Test
    public void forEachShouldNotPassEntityAfterComponentUnregister_SingleQuery()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Consumer<Entity> mockConsumer = mock(Consumer.class);
        SubDummyComponent queriedComponent = new SubDummyComponent();

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, new DummyComponent());
        entitySystem.registerComponent(mockEntity, queriedComponent);
        entitySystem.register(mockEntityNoise);
        entitySystem.registerComponent(mockEntityNoise, new DummyComponent());

        entitySystem.unregisterComponent(mockEntity, queriedComponent);

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, SubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, never()).accept(any());
    }

    @Test
    public void forEachShouldNotPassEntityAfterComponentTypeUnregister_SingleQuery()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Consumer<Entity> mockConsumer = mock(Consumer.class);

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, new DummyComponent());
        entitySystem.registerComponent(mockEntity, new SubDummyComponent());
        entitySystem.register(mockEntityNoise);
        entitySystem.registerComponent(mockEntityNoise, new DummyComponent());

        entitySystem.unregisterComponentType(mockEntity, SubDummyComponent.class);

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, SubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, never()).accept(any());
    }

    @Test
    public void forEachShouldNotPassEntityAfterComponentUnregister_MultipleQuery()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Consumer<Entity> mockConsumer = mock(Consumer.class);
        SubDummyComponent queriedComponentOne = new SubDummyComponent();
        SubSubDummyComponent queriedComponentTwo = new SubSubDummyComponent();

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, new DummyComponent());
        entitySystem.registerComponent(mockEntity, queriedComponentOne);
        entitySystem.registerComponent(mockEntity, queriedComponentTwo);
        entitySystem.register(mockEntityNoise);
        entitySystem.registerComponent(mockEntityNoise, new DummyComponent());

        entitySystem.unregisterComponent(mockEntity, queriedComponentOne);

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, SubDummyComponent.class, SubSubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, never()).accept(any());
    }

    @Test
    public void forEachShouldNotPassEntityAfterComponentTypeUnregister_MultipleQuery()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Consumer<Entity> mockConsumer = mock(Consumer.class);
        SubDummyComponent queriedComponentOne = new SubDummyComponent();
        SubSubDummyComponent queriedComponentTwo = new SubSubDummyComponent();

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, new DummyComponent());
        entitySystem.registerComponent(mockEntity, queriedComponentOne);
        entitySystem.registerComponent(mockEntity, queriedComponentTwo);
        entitySystem.register(mockEntityNoise);
        entitySystem.registerComponent(mockEntityNoise, new DummyComponent());

        entitySystem.unregisterComponentType(mockEntity, SubSubDummyComponent.class);

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, SubDummyComponent.class, SubSubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, never()).accept(any());
    }

    @Test
    public void forEachShouldFilterEntitiesCorrectly_SingleQuery() {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        SubDummyComponent subDummyC = new SubDummyComponent();
        SubSubDummyComponent subSubDummyC = new SubSubDummyComponent();

        Consumer<Entity> mockConsumer = mock(Consumer.class);

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, subDummyC);
        entitySystem.register(mockEntityNoise);
        entitySystem.registerComponent(mockEntityNoise, subSubDummyC);

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, SubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, never()).accept(mockEntityNoise);
        verify(mockConsumer, times(1)).accept(mockEntity);
    }

    @Test
    public void forEachShouldFilterEntitiesCorrectly_MultipleQuery() {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        Entity mockEntityNoise = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        DummyComponent dummyC = new DummyComponent();
        SubDummyComponent subDummyC = new SubDummyComponent();
        SubSubDummyComponent subSubDummyC = new SubSubDummyComponent();

        Consumer<Entity> mockConsumer = mock(Consumer.class);

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, dummyC);
        entitySystem.registerComponent(mockEntity, subDummyC);
        entitySystem.registerComponent(mockEntity, subSubDummyC);
        entitySystem.register(mockEntityNoise);
        entitySystem.registerComponent(mockEntityNoise, dummyC);
        entitySystem.registerComponent(mockEntityNoise, subDummyC);

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, DummyComponent.class, SubDummyComponent.class, SubSubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, never()).accept(mockEntityNoise);
        verify(mockConsumer, times(1)).accept(mockEntity);
    }

    @Test
    public void forEachShouldNotInvokeIfNoEntityHasQueriedComponentType_MultipleQuery() {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();

        Entity mockEntity = mock(Entity.class);
        when(mockEntity.getEntitySystem()).thenReturn(entitySystem);

        DummyComponent dummyC = new DummyComponent();

        Consumer<Entity> mockConsumer = mock(Consumer.class);

        // ACT
        entitySystem.register(mockEntity);
        entitySystem.registerComponent(mockEntity, dummyC);

        entitySystem.each(e -> { mockConsumer.accept(e); return Unit.INSTANCE; }, DummyComponent.class, SubDummyComponent.class);

        // VERIFY
        verify(mockConsumer, never()).accept(any());
    }

    @Test
    public void testExecutionOrder()
    {
        // SETUP
        EntitySystem entitySystem = new EntitySystem();
        DummySystemA dummySystemA = new DummySystemA();
        DummySystemB dummySystemB = new DummySystemB();
        entitySystem.register(dummySystemA);
        entitySystem.register(dummySystemB);

        entitySystem.assureExecutionOrder(DummySystemA.class, DummySystemB.class);

        // ACT
        entitySystem.tick(.1f);

        // VERIFY
        assertNotEquals("System A was never called", -1, dummySystemA.lastCalledAt);
        assertNotEquals("System B was never called", -1, dummySystemB.lastCalledAt);
        assertTrue("Execution order was not kept, B called before A", dummySystemA.lastCalledAt <= dummySystemB.lastCalledAt);

        // change the execution order
        entitySystem.assureExecutionOrder(DummySystemB.class, DummySystemA.class);
        // and reset the dummy systems
        dummySystemA.lastCalledAt = -1;
        dummySystemB.lastCalledAt = -1;

        // ACT
        entitySystem.tick(.1f);

        // VERIFY
        assertNotEquals("System A was never called", -1, dummySystemA.lastCalledAt);
        assertNotEquals("System B was never called", -1, dummySystemB.lastCalledAt);
        assertTrue("Execution order was not kept, A called before B", dummySystemA.lastCalledAt >= dummySystemB.lastCalledAt);
    }

    @Test
    public void testComponentOnRegisterIsCalled()
    {
        // SETUP
        Component mockComponent = mock(Component.class);
        doReturn(Component.class).when(mockComponent).getType();
        EntitySystem entitySystem = new EntitySystem();
        Entity entity = new BaseEntity(entitySystem);
        entitySystem.register(entity);

        // ACT
        entitySystem.registerComponent(entity, mockComponent);

        // VERIFY
        verify(mockComponent).onRegister(entity);
    }

    @Test
    public void testComponentOnUnregisterIsCalled()
    {
        // SETUP
        Component mockComponent = mock(Component.class);
        doReturn(Component.class).when(mockComponent).getType();
        EntitySystem entitySystem = new EntitySystem();
        Entity entity = new BaseEntity(entitySystem);
        entitySystem.register(entity);
        entitySystem.registerComponent(entity, mockComponent);

        // ACT
        entitySystem.unregisterComponent(entity, mockComponent);

        // VERIFY
        verify(mockComponent).onUnregister(entity);
    }

    /**
     * Used by {@link #testExecutionOrder()}
     */
    private static class DummySystemA implements ComponentSystem
    {
        public long lastCalledAt = -1;

        public void tick(EntitySystem s, float duration)
        {
            lastCalledAt = System.currentTimeMillis();

            // sleep for a third of the tick duration
            try
            {
                Thread.sleep((int) (333 * duration));
            } catch (InterruptedException ignored) {}
        }

        @Override
        public void onRegister(@NotNull EntitySystem es) {}

        @Override
        public void onUnregister(@NotNull EntitySystem es) {}
    }

    /**
     * Used by {@link #testExecutionOrder()}
     */
    private static class DummySystemB implements ComponentSystem
    {
        public long lastCalledAt = -1;

        public void tick(EntitySystem s, float duration)
        {
            lastCalledAt = System.currentTimeMillis();

            // sleep for a third of the tick duration
            try
            {
                Thread.sleep((int) (333 * duration));
            } catch (InterruptedException ignored) {}
        }

        @Override
        public void onRegister(@NotNull EntitySystem es) {}

        @Override
        public void onUnregister(@NotNull EntitySystem es) {}
    }

    private static class DummyComponent implements Component
    {
        @Override
        public Class<?> getType() { return this.getClass(); }

        @Override
        public void onUnregister(@NotNull Entity e)
        {

        }

        @Override
        public void onRegister(@NotNull Entity e)
        {

        }
    }

    private static class SubDummyComponent extends DummyComponent
    {}

    private static class SubSubDummyComponent extends DummyComponent
    {}
}
