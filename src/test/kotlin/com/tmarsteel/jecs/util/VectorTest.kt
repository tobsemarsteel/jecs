package com.tmarsteel.jecs.util

import junit.framework.TestCase.assertEquals
import nl.jqno.equalsverifier.EqualsVerifier
import org.junit.Test

/**
 * @author Tobias Marstaller
 */
class VectorTest {
    @Test
    @Throws(Exception::class)
    fun testInverse() {
        val v = Vector(2.0, 3.0, 4.0).inverse()

        assertEquals("Inverse yielded invalid result for x", -2.0, v.x)
        assertEquals("Inverse yielded invalid result for y", -3.0, v.y)
        assertEquals("Inverse yielded invalid result for z", -4.0, v.z)
    }

    @Test
    @Throws(Exception::class)
    fun testAdd() {
        val v1 = Vector(2.0, 3.0, -4.0)
        val v2 = Vector(5.0, -1.0, 2.0)

        val v = v1.add(v2)

        assertEquals("add yielded invalid result for x", 7.0, v.x)
        assertEquals("add yielded invalid result for y", 2.0, v.y)
        assertEquals("add yielded invalid result for z", -2.0, v.z)
    }

    @Test
    @Throws(Exception::class)
    fun testSubtract() {
        val v1 = Vector(2.0, 3.0, -4.0)
        val v2 = Vector(5.0, -1.0, 2.0)

        val v = v1.subtract(v2)

        assertEquals("subtract yielded invalid result for x", -3.0, v.x)
        assertEquals("subtract yielded invalid result for y", 4.0, v.y)
        assertEquals("subtract yielded invalid result for z", -6.0, v.z)
    }

    @Test
    @Throws(Exception::class)
    fun testMultiply() {
        var v = Vector(2.0, 3.0, 4.0)

        v = v.multiply(2.0)

        assertEquals("multiply yielded invalid result for x", 4.0, v.x)
        assertEquals("multiply yielded invalid result for y", 6.0, v.y)
        assertEquals("multiply yielded invalid result for z", 8.0, v.z)
    }

    @Test
    @Throws(Exception::class)
    fun testAbs() {
        val v = Vector(5.0, -2.0, 1.0)

        val result = Math.sqrt(25 + 4 + 1.toDouble())

        assertEquals("abs yielded invalid result ", result, v.length)
    }

    @Test
    fun testEqualsAndHashCode() {
        EqualsVerifier.forClass(Vector::class.java).verify()
    }
}