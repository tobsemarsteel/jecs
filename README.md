# jECS

An efficient [Entity Component System](https://en.wikipedia.org/wiki/Entity_component_system) designed for simple games

## Setup

Checkout this repository or download the current release branch. Dependencies can be installed using with `maven install`.

## Usage

Tutorials and guidelines on how to program software using this framework can be found in the Wiki. This repository contains the very core framework only. See these repositories for useful standard components and systems:

* [Health](//bitbucket.org/tobsemarsteel/jecs-health)
* [Motion](//bitbucket.org/tobsemarsteel/jecs-motion)

## Contribution guidelines

Feel free to fork and create pull-requests; i'll integrate important things into the core framework. However, additional systems and components should be placed into separate repositories to keep this one small.

These are my requirements as to architecture and quality of code for this framework:

### Architectual

* All the state-of-the art defaults: Composition over Inheritance, Single Responsibility Principle, DRY, YAGNI
* Eventbased
    * Components do not have events or listeners; all events are triggered from the corresponding systems
        * Domain-Specific events should be registered with your domain entity classes; those should redirect the listeners to the system. That leads to neat and easy-to-use domain classes and keeps the component classes very simple.
    * As soon as Kotlin 1.1 is available, all Listeners should be changed `typealias`ed function types instaed of SAM interfaces.
* Use properties with get() and set() implementations wherever possible. Create functions only for things that *acutally make something happen* (instead of just changing a value).

### Code quality wise

* All functionalities are unit-tested
    * Systems apply all changes to components **correctly**
    * Systems call all listeners appropriately
* Doc-Comments **must** be on **all** `public` and `protected` functions; they should be on `private` ones, too.
    * Declare all value-ranges for parameters (e.g. int greater than 0 and less than 512)
    * Declare all thrown Exceptions and under which conditions they are thrown; especially for `IllegalArgumentException`s
        * The more `assert()`s, the better.